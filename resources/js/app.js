/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

window.Vue = require('vue')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import ChatApplication from './components/ChatApplication.vue'
import Echo from 'laravel-echo'

const app = new Vue({
  el: '#app',
  data: {
    userID: null
  },
  components: {
    'chat-application' : ChatApplication
  },
  mounted () {
    // Assign the ID from meta tag for future use in application
    this.userID = document.head.querySelector('meta[name="userID"]').content
  }
})

window._ = require('lodash')
window.Popper = require('popper.js').default
window.io = require('socket.io-client')

